// HTTP Server
const http = require("http");
// import dotenv module
require("dotenv").config();
// menggunakan dotenv untuk mengatur port
const PORT = process.env.PORT;
// import datajson
const dataJSON = require("./datas.json");

const fs = require("fs");
const path = require("path");
const PUBLIC_DIRECTORY = path.join(__dirname, "public");

// fungsi memanggil html
function getHTML(htmlFileName) {
  const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
  return fs.readFileSync(htmlFilePath, "utf-8");
}

// fungsi untuk json menjadi string
function toJSON(value) {
  return JSON.stringify(value);
}

function onRequest(req, res) {
  switch (req.url) {
    // case untuk memanggil CSS file
    case "/assets/styles.css":
      res.writeHead(200, { "Content-Type": "text/css" });
      const fileContent = fs.readFileSync("./public/assets/styles.css", {
        encoding: "utf-8",
      });
      res.write(fileContent);
      res.end();
      return;

    // case untuk memanggil file gambar
    case "/assets/img/logo.png":
      res.writeHead(200, { "Content-Type": "image/png" });
      const assetContent = fs.readFileSync("./public/assets/img/logo.png");
      res.write(assetContent);
      res.end();
      return;

    // case untuk homepage
    case "/":
      res.writeHead(200);
      res.end(getHTML("home.html"));
      return;

    // case untuk aboutpage
    case "/about":
      res.writeHead(200);
      res.end(getHTML("about.html"));
      return;

    // case untuk URL/API data 1
    case "/data1":
      // filter data sesuai kondisi yang diminta
      const data1 = dataJSON.filter((data) => data.age < 30 && data.favoriteFruit === "banana");
      // jika tidak ada datang yang sesuai kondisi
      if (data1.length === 0) {
        const notFoundJSON = toJSON({
          status: "FAIL",
          message: "data tidak ada / tidak ditemukan",
        });
        res.setHeader("Content-Type", "application/json");
        res.writeHead(404);
        res.end(notFoundJSON);
        return;
      }
      // mengubah data 1 menjadi string
      const responseJSON1 = toJSON(data1);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(responseJSON1);
      return;

    // Case untuk URL/API data 2
    case "/data2":
      // filter data sesuai kondisi yang diminta
      const data2 = dataJSON.filter((data) => (data.gender === "female" || data.company === "FSW4") && data.age > 30);
      // jika tidak ada yang sesuai kondisi
      if (data2.length === 0) {
        const notFoundJSON = toJSON({
          status: "FAIL",
          message: "data tidak ada / tidak ditemukan",
        });
        res.setHeader("Content-Type", "application/json");
        res.writeHead(404);
        res.end(notFoundJSON);
        return;
      }
      // mengubah data 2 menjadi string
      const responseJSON2 = toJSON(data2);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(responseJSON2);
      return;

    // case untuk URL/API data 3
    case "/data3":
      // filter data sesuai kondisi yang diminta
      const data3 = dataJSON.filter((data) => data.eyeColor === "blue" && data.age >= 35 && data.age <= 40 && data.favoriteFruit === "apple");
      // jika tidak ada data yang sesuai kondisi
      if (data3.length === 0) {
        const notFoundJSON = toJSON({
          status: "FAIL",
          message: "data tidak ada / tidak ditemukan",
        });
        res.setHeader("Content-Type", "application/json");
        res.writeHead(404);
        res.end(notFoundJSON);
        return;
      }
      // ubah data3 menjadi string
      const responseJSON3 = toJSON(data3);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(responseJSON3);
      return;

    // case untuk URL/API data 4
    case "/data4":
      // filter data sesuai kondisi yang diminta
      const data4 = dataJSON.filter((data) => (data.company === "Pelangi" || data.company === "Intel") && data.eyeColor === "green");
      // jika tidak ada data yang sesuai kondisi
      if (data4.length === 0) {
        const notFoundJSON = toJSON({
          status: "FAIL",
          message: "data tidak ada / tidak ditemukan",
        });
        res.setHeader("Content-Type", "application/json");
        res.writeHead(404);
        res.end(notFoundJSON);
        return;
      }
      // ubah data 4 menjadi string
      const responseJSON4 = toJSON(data4);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(responseJSON4);
      return;

    // case untuk URL/API data 5
    case "/data5":
      // filter data sesuai kondisi yang diminta
      const data5 = dataJSON.filter((data) => data.registered <= "2016-01-01" && data.isActive === true);
      // jika tidak ada yang sesuai kondisi
      if (data5.length === 0) {
        const notFoundJSON = toJSON({
          status: "FAIL",
          message: "data tidak ada / tidak ditemukan",
        });
        res.setHeader("Content-Type", "application/json");
        res.writeHead(404);
        res.end(notFoundJSON);
        return;
      }
      // ubah data 5 menjadi string
      const responseJSON5 = toJSON(data5);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(responseJSON5);

      return;

    default:
      // jika URL tidak ditemukan
      res.writeHead(404);
      res.end(getHTML("404.html"));
      return;
  }
}

const server = http.createServer(onRequest);

// Jalankan server
server.listen(PORT, "0.0.0.0", () => {
  console.log("Server sudah berjalan, silahkan buka http://0.0.0.0:%d", PORT);
});
